graphviz-watcher
================

Summary
-------

graphviz-watcher is an application that displays graphviz dot-files.
The view is refreshed whenever the file is updated. It is possible to
use different graphviz engines.

It is written using C++, Qt and QML. You will need graphviz installed
to use it.

Installation
------------

To compile graphviz-watcher, you will need Qt SDK. Compiling is done
running following commands.

    qmake
    make

If you are on Linux (or anything other that installs stuff to /usr/local/bin)
you can run following command to install the application

    sudo make install

Licence
-------
See LICENSE.txt
