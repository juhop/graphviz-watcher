/*
 * author: Juho Peltonen
 * license: See LICENSE.txt
 */

#include <QApplication>
#include <QtDeclarative>
#include "viewer.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QDeclarativeView viewer;
    qmlRegisterType<Viewer>("graphviz", 1, 0, "GraphvizViewer");
    viewer.setSource(QUrl("qrc:///qml/main.qml"));

    viewer.setResizeMode(QDeclarativeView::SizeRootObjectToView);
    viewer.rootObject()->setProperty("dotfile", argc == 2 ? argv[1] : "");
    viewer.showNormal();

    return app.exec();
}
