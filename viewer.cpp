/*
 * author: Juho Peltonen
 * license: See LICENSE.txt
 */

#include "viewer.h"

#include <QPainter>
#include <QProcess>
#include <QFileSystemWatcher>
#include <algorithm>
#include <QFile>
#include <QFileInfo>

Viewer::Viewer(QDeclarativeItem *parent) :
    QDeclarativeItem(parent)
{
    setFlag(QDeclarativeItem::ItemHasNoContents, false);
    dot_engine_name = "dot";

    process = new QProcess(this);
    connect(process, SIGNAL(finished(int)), this, SLOT(processExit(int)));

    file_watcher = new QFileSystemWatcher(this);
    connect(file_watcher, SIGNAL(directoryChanged(QString)), this, SLOT(updateImage(QString)));
}

void Viewer::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    if (svg_renderer.isValid()) {
        QSize size = svg_renderer.defaultSize();
        qreal scale = std::min(width() / size.width(), height() / size.height());

        // scale so that the graphics fill as much as possible
        painter->scale(scale, scale);

        // translate so that the graphics will be rendered in to middle
        painter->translate(0.5 * (width() / scale - size.width()), 0);

        svg_renderer.render(painter, QRectF(0, 0, size.width(), size.height()));
    }
}

void Viewer::setFile(const QString &file)
{
    dotfile_path = file;
    if (file.isEmpty())
        return;
    updateImage();

    // Set the file watcher to look for changes in containing directory.
    // This is done, because if the file is temporary destroyed, we can still observe
    file_watcher->removePaths(file_watcher->directories());
    QFileInfo info(file);
    file_watcher->addPath(info.absolutePath());
    modify_time.setMSecsSinceEpoch(1);
}

void Viewer::updateImage(QString /*dir*/)
{
    // before doing anything, check that the file exists and that it has been modified
    if (QFile::exists(dotfile_path)) {
        QFileInfo info(dotfile_path);
        QDateTime modified = info.lastModified();
        if (modified > modify_time)
            updateImage();
        modify_time = modified;
    }
}

void Viewer::updateImage()
{
    QStringList args;
    args.append("-Tsvg");
    args.append(dotfile_path);
    process->start(dot_engine_name, args);
}

void Viewer::processExit(int status)
{
    if (status == 0) {
        svg_renderer.load(process->readAllStandardOutput());
        setErrors("");
    } else {
        setErrors(QString::fromUtf8(process->readAllStandardError()));
    }
    update();
}

void Viewer::setErrors(const QString s)
{
    if (s == error_messages)
        return;
    error_messages = s;
    emit errorsChanged();
}

void Viewer::setEngine(const QString& engine)
{
    dot_engine_name = engine;
    emit engineChanged();
    updateImage();
}
