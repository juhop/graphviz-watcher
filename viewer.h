/*
 * author: Juho Peltonen
 * license: See LICENSE.txt
 */

#ifndef VIEWER_H
#define VIEWER_H

#include <QDeclarativeItem>
#include <QtSvg/QSvgRenderer>
#include <QDateTime>

class QProcess;
class QFileSystemWatcher;

class Viewer : public QDeclarativeItem
{
    Q_OBJECT
    Q_PROPERTY(QString file READ file WRITE setFile)
    Q_PROPERTY(QString errors READ errors NOTIFY errorsChanged)
    Q_PROPERTY(QString engine READ engine NOTIFY engineChanged WRITE setEngine)
public:
    explicit Viewer(QDeclarativeItem *parent = 0);

protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *);

    void setFile(const QString& file);
    QString file() { return dotfile_path; }
    QString errors() { return error_messages; }
    void setEngine(const QString& engine);
    QString engine() { return dot_engine_name; }

private:
    void setErrors(const QString s);
    QString dotfile_path;
    QString dot_engine_name;
    QDateTime modify_time;
    QString error_messages;
    QSvgRenderer svg_renderer;
    QFileSystemWatcher* file_watcher;
    QProcess* process;

signals:
    void errorsChanged();
    void engineChanged();

private slots:
    void updateImage();
    void updateImage(QString dir);
    void processExit(int status);
    
};

#endif // VIEWER_H
