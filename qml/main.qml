/*
 * author: Juho Peltonen
 * license: See LICENSE.txt
 */

import QtQuick 1.1
import graphviz 1.0

Rectangle {
    width: 500
    height: 500
    property string dotfile

    Item {
        id: engine_selection
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
        }
        height: 24
        Row {
            Text {
                anchors.verticalCenter: parent.verticalCenter
                text: 'engine: '
            }
            Repeater {
                model: ['dot', 'neato', 'twopi', 'circo', 'fdp', 'sfdp']
                Rectangle {
                    height: engine_selection.height - 4
                    color: viewer.engine === modelData ? '#ccc' : '#888'
                    border.color: 'black'
                    width: 60
                    Text {
                        text: modelData
                        anchors.centerIn: parent
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: viewer.engine = modelData
                    }
                }
            }
        }
    }
    Item {
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            top: engine_selection.bottom
        }
        GraphvizViewer {
            id: viewer
            anchors.fill: parent
            file: dotfile
        }
        Rectangle {
            color: '#44444444'
            width: error_view.width
            height: error_view.height
        }
        Text {
            id: error_view
            font.pixelSize: 16
            text: viewer.errors
        }
    }
}
