TEMPLATE = app
TARGET = graphviz-watcher
DEPENDPATH += .
INCLUDEPATH += .

target.path = /usr/local/bin/
INSTALLS += target

QT += declarative svg

HEADERS += viewer.h
SOURCES += main.cpp viewer.cpp
RESOURCES += qml.rc

OTHER_FILES += qml/main.qml
